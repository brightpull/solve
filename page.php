<?php get_header(); ?>

<div class="bg-gray-400 py-32 hide-print" style="background: linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)), url('<?php echo solve_hero_img_url(); ?>'); background-position: center; background-size: cover;">

	<div class="container">

		<h1 class="text-white mb-0 md:text-6xl"><?php echo get_the_title(solve_ancestor_id()); ?></h1>

	</div>

</div>

<div class="bg-gray-200 py-4 text-gray-700 text-sm hide-print">

	<div class="container flex items-center justify-between">

		<p class="mb-0">
			<a href="<?php echo site_url(); ?>">Home</a> >
			<?php if ($post->post_parent) : ?>
				<a href="<?php echo get_permalink( $post->post_parent ); ?>"><?php echo get_the_title($post->post_parent); ?></a> >
			<?php endif; ?>
			<?php the_title(); ?>
		</p>

		<ul class="flex items-center mb-0">
			<li class="mr-6">
				<a href="javascript:window.print();">
					<i class="fas fa-print mr-2"></i>Print this page
				</a>
			</li>
			<li>
				<a href="javascript: imageToPdf()">
					<i class="fas fa-file-pdf mr-2"></i>PDF
				</a>
			</li>
		</ul>

	</div>

</div>

<div class="container py-20">

	<div class="grid-sidebar">

		<div class="content-area">

			<?php if ( have_posts() ) : ?>

			    <?php while ( have_posts() ) : the_post(); ?>

					<h1><?php the_title(); ?></h1>

					<?php the_content(); ?>

					<?php if (get_field('publish_date')) : ?>
						<p class="mt-6 text-green font-bold">Page updated: <?php the_field('publish_date'); ?></p>
					<?php endif; ?>

			    <?php endwhile; ?>

			<?php endif; ?>

			<?php if (is_page(73)) : ?>

				<div class="grid-4">

					<?php if( have_rows('annual_reports') ): ?>

						<h4 class="mt-20">Annual Reports</h4>

					 	<?php  while ( have_rows('annual_reports') ) : the_row(); ?>

							<div class="text-center">

								<p class="text-blue font-bold mb-0"><?php the_sub_field('title'); ?></p>

								<a class="my-2 mt-1 mb-3 block" href="<?php the_sub_field('pdf'); ?>">
									<img src="<?php the_sub_field('thumbnail'); ?>" alt="pdf thumbnail">
								</a>

								<a class="text-blue text-sm" style="text-decoration: none" href="<?php the_sub_field('pdf'); ?>">
									Download PDF
								</a>

							</div>

					    <?php endwhile; ?>

					<?php endif; ?>

				</div>

			<?php endif; ?>

		</div>

		<?php get_sidebar(); ?>

	</div>

</div>

<?php get_footer(); ?>
