<?php

/* Template Name: Projects */

get_header(); ?>

<div class="bg-gray-400 py-32" style="background: linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)), url('<?php echo solve_hero_img_url(); ?>'); background-position: center; background-size: cover;">

	<div class="container">

		<h1 class="text-white mb-0 md:text-6xl">Equipment Solutions</h1>

	</div>

</div>

<div class="bg-gray-200">

	<div class="container py-20 grid-sidebar">

		<div>

			<?php if ( have_posts() ) : ?>

			    <?php while ( have_posts() ) : the_post(); ?>

			        <?php the_content(); ?>

			    <?php endwhile; ?>

			<?php endif; ?>

		</div>

		<div>

			<?php $category_id = $_GET['category']; ?>

			<?php $categories = get_categories('category'); ?>

			<h4>Filter by Category</h4>

			<form class="category-select" action="#" method="get">
				<select class="jq-category" name="category">
					<option value="?category=0">Select one...</option>
					<?php foreach ($categories as $category) : ?>
						<option class="category-select__category" value="?category=<?php echo $category->term_id; ?>" <?php if ($category->term_id == $category_id) { echo 'selected'; } ?>><?php echo $category->name; ?></option>
					<?php endforeach; ?>
				</select>
			</form>

		</div>

	</div>

</div>

<div class="container py-20">

	<?php if ($category_id) : ?>
		<?php query_posts( 'posts_per_page=100&post_type=project&cat=' . $category_id ); ?>
	<?php else : ?>
		<?php query_posts( 'posts_per_page=100&post_type=project' ); ?>
	<?php endif; ?>

	<?php if ( have_posts() ) : ?>

		<div class="grid-3">

		    <?php while ( have_posts() ) : the_post(); ?>

				<?php echo get_template_part( 'parts/project' ); ?>

		    <?php endwhile; ?>

		</div>

	<?php endif; ?>

	<?php wp_reset_query(); ?>

	<p class="mb-0">
		<?php previous_posts_link( 'Newer posts' ); ?>
		<?php next_posts_link( 'Older posts' ); ?>
	</p>

</div>

<?php get_footer(); ?>
