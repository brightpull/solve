<?php

/*
|--------------------------------------
| Include files
|--------------------------------------
*/
get_template_part( 'includes/cleanup' );
get_template_part( 'includes/cpt' );
get_template_part( 'includes/helpers' );
get_template_part( 'includes/shortcodes' );

/*
|--------------------------------------
| Add theme support
|--------------------------------------
*/
add_theme_support( 'post-thumbnails' );
add_theme_support( 'title-tag' );

/*
|--------------------------------------
| Enqueue scripts
|--------------------------------------
*/
function solve_load_assets()
{
    // jQuery
    if ( !is_admin() ) { wp_deregister_script( 'jquery' ); }
    wp_enqueue_script( 'jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js', array(), '3.3.1', false );

    // CSS
    wp_enqueue_style( 'aos', 'https://unpkg.com/aos@next/dist/aos.css' );
    wp_enqueue_style( 'app', get_template_directory_uri() . '/style.css' );

    // JavaScript
    wp_enqueue_script( 'aos', 'https://unpkg.com/aos@next/dist/aos.js', array(), '3.0.0', true );
    wp_enqueue_script( 'slick', get_template_directory_uri() . '/js/slick.min.js', array(), '1.8.1', true );
    wp_enqueue_script( 'js-cookie', 'https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.2.0/js.cookie.min.js', array(), '2.2.0', true );
    wp_enqueue_script( 'app', get_template_directory_uri() . '/js/app.min.js', array(), null, true );
}
add_action( 'wp_enqueue_scripts', 'solve_load_assets' );

/*
|--------------------------------------
| Remove image sizes
|--------------------------------------
*/
function solve_remove_image_sizes( $sizes )
{
    unset( $sizes['small'] );
    unset( $sizes['medium'] );
    unset( $sizes['large'] );
    return $sizes;
}
add_filter( 'intermediate_image_sizes_advanced', 'solve_remove_image_sizes' );

/*
|--------------------------------------
| Add Volunteer role
|--------------------------------------
*/
add_role('volunteer', __( 'Volunteer' ), array(
    'read' => true,
    'edit_posts' => true,
    'delete_posts' => false
));

/*
|--------------------------------------
| Custom pagination styles
|--------------------------------------
*/
function posts_link_attributes_1() {
    return 'class="button bg-blue mt-10 px-6 py-3"';
}
function posts_link_attributes_2() {
    return 'class="button border border-green text-green mt-10 mr-2 px-6 py-3"';
}
add_filter('next_posts_link_attributes', 'posts_link_attributes_1');
add_filter('previous_posts_link_attributes', 'posts_link_attributes_2');

/*
|--------------------------------------
| Shorten excerpt length
|--------------------------------------
*/
function custom_excerpt_length( $length ) {
	return 25;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
