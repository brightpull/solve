<?php get_header(); ?>

<div class="bg-gray-400 py-32" style="background: linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)), url('<?php echo get_the_post_thumbnail_url( 66 ); ?>'); background-position: center; background-size: cover;">

	<div class="container">

		<h1 class="text-white mb-0 md:text-6xl">Equipment Solutions</h1>

	</div>

</div>

<div class="container py-20">

	<div class="grid-2">

		<div class="content-area">

			<?php if ( have_posts() ) : ?>

			    <?php while ( have_posts() ) : the_post(); ?>

					<?php the_post_thumbnail( 'full' ); ?>

			    <?php endwhile; ?>

			<?php endif; ?>

		</div>

		<div class="content-area">

			<p><a href="<?php echo get_permalink( 66 ); ?>">&larr; Back to Equipment Solutions</a></p>

			<h1><?php the_title(); ?></h1>

			<?php the_content(); ?>

		</div>

	</div>

</div>

<?php get_footer(); ?>
