<?php

/*
|--------------------------------------
| Buttons
|--------------------------------------
*/
function solve_button_shortcode( $atts = array() )
{
	extract(shortcode_atts(array(
		'text' => 'Button Text',
		'link' => '#',
		'color' => 'blue',
	), $atts));

	return '<a href="' . $link . '" class="button--' . $color . ' mr-2">' . $text . '</a>';
}

add_shortcode('button', 'solve_button_shortcode');
