<?php

/*
|--------------------------------------
| Ancestor ID
|--------------------------------------
*/
function solve_ancestor_id()
{
    global $post;

    if ( $post->post_parent ) {
        $ancestors = array_reverse( get_post_ancestors( $post->ID ) );
        return $ancestors[0];
    }

    return $post->ID;
}

/*
|--------------------------------------
| Hero background URL
|--------------------------------------
*/
function solve_hero_img_url()
{
    global $post;

    if ( is_search() ) {
        return get_template_directory_uri() . '/images/hero.jpg';
    }

    // Does this post have a featured image?
    if ( get_the_post_thumbnail_url( $post->ID ) ) {
        return get_the_post_thumbnail_url( $post->ID, 'full' );
    }

    // Does it’s ancestor have one?
    if ( get_the_post_thumbnail_url( solve_ancestor_id() ) ) {
        return get_the_post_thumbnail_url( solve_ancestor_id(), 'full' );
    }

    // No? Fall back to default hero image
    return get_template_directory_uri() . '/images/hero.jpg';
}
