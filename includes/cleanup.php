<?php

/*
|--------------------------------------
| Hide admin bar
|--------------------------------------
*/
show_admin_bar( false );

/*
|--------------------------------------
| Superfluous code
|--------------------------------------
*/
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'wp_generator' );

/*
|--------------------------------------
| Emoji scripts
|--------------------------------------
*/
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

/*
|--------------------------------------
| Hide WP version
|--------------------------------------
*/
function solve_remove_wp_version()
{
    return '';
}
add_filter( 'the_generator', 'solve_remove_wp_version' );

/*
|--------------------------------------
| Hide dashboard menu items
|--------------------------------------
*/
function solve_remove_admin_pages()
{
    // remove_menu_page( 'upload.php' );
    // remove_menu_page( 'edit-comments.php' );
    // remove_menu_page( 'users.php' );
    // remove_menu_page( 'tools.php' );
    // remove_menu_page( 'plugins.php' );
    // remove_menu_page( 'options-general.php' );
    // remove_menu_page( 'themes.php' );
    // remove_menu_page( 'edit.php?post_type=acf-field-group' );
}
add_action( 'admin_menu', 'solve_remove_admin_pages' );
