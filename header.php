<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover">

    <meta name="author" content="Tim Martin">

    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">

    <!-- Fonts -->
    <script src="https://kit.fontawesome.com/8fd5aaf580.js"></script>

    <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
    <![endif]-->

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<a href="<?php echo site_url(); ?>" class="svg-logo show-print mt-16">
    <div class="mb-6">
        <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="logo">
    </div>
    <span><?php the_time('F j, Y'); ?></span>
</a>

<?php echo get_template_part( 'parts/feedback' ); ?>

<?php echo get_template_part( 'parts/search' ); ?>

<?php echo get_template_part( 'parts/newsletter-form' ); ?>

<?php echo get_template_part( 'parts/toolbar' ); ?>

<?php echo get_template_part( 'parts/menu' ); ?>
