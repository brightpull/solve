<?php get_header(); ?>

<?php echo get_template_part( 'parts/hero' ); ?>

<div class="bg-gray-200 py-20" style="margin-top: 0px;">

	<?php echo get_template_part( 'parts/four-boxes' ); ?>

	<?php echo get_template_part( 'parts/what-we-do' ); ?>

</div>

<?php echo get_template_part( 'parts/projects' ); ?>

<div class="bg-white py-20">

	<?php echo get_template_part( 'parts/latest-news' ); ?>

	<?php echo get_template_part( 'parts/promotion' ); ?>

</div>

<div class="container">

	<h1 class="leading-none">Our Partners</h1>

	<p><?php the_field('partners_introduction', 11); ?></p>

	<div class="grid-4-all mb-16 pb-16 border-b border-gray-600">

		<?php if ( have_rows('partners_logos', 11) ) : ?>

			<?php while ( have_rows('partners_logos', 11) ) : the_row(); ?>

				<a class="block h-32" href="<?php the_sub_field('url'); ?>" target="_blank" style="background-image: url('<?php the_sub_field('logo'); ?>'); background-size: contain; background-position: center center; background-repeat: no-repeat;;"></a>

			<?php endwhile; ?>

			<?php else : ?>

		<?php endif; ?>

	</div>

	<div class="text-center">

		<h3 class="mb-12 leading-none">Our Sponsors</h3>

	</div>

	<div class="grid-6 pb-16 sponsors">

		<?php if ( have_rows('sponsors_logos', 11) ) : ?>

			<?php while ( have_rows('sponsors_logos', 11) ) : the_row(); ?>

				<a class="block h-24" href="<?php the_sub_field('url'); ?>" target="_blank" style="background-image: url('<?php the_sub_field('logo'); ?>'); background-size: contain; background-position: center center; background-repeat: no-repeat;;"></a>

			<?php endwhile; ?>

			<?php else : ?>

		<?php endif; ?>

	</div>

</div>

<div class="py-16 lg:py-24" style="background-image: url('https://freedomwheels.org.au/wp-content/themes/freedom-wheels/dist/images/tad-banner-bg.jpg');">

	<div class="container">

		<img class="mb-8" width="400" src="https://freedomwheels.org.au/wp-content/themes/freedom-wheels/dist/images/logos/logo-tad-australia.svg" alt="tad australia">

		<p class="text-white text-lg mb-12 md:w-1/2">
			Have you heard about TAD Australia's other solutions? TAD Australia is the federation of seven state-based non-profit, assistive technology providers. We have a dedicated group of staff and volunteers who design and build customised assistive technology and equipment solutions for those living with a disability.
		</p>

		<a href="http://tadaustralia.org.au" target="_blank" class="button--blue">
			Learn More
		</a>

	</div>

</div>

<?php get_footer(); ?>
