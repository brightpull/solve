<svg id="Group_3455" data-name="Group 3455" xmlns="http://www.w3.org/2000/svg" width="58.367" height="58.367" viewBox="0 0 58.367 58.367">
  <g id="Group_3443" data-name="Group 3443" transform="translate(4.863 10.943)">
    <path id="Path_69" data-name="Path 69" d="M487.864,447.228a1.222,1.222,0,0,1-.861-.355l-3.648-3.648a1.218,1.218,0,0,1,1.722-1.722l2.787,2.789,6.435-6.437a1.218,1.218,0,0,1,1.722,1.722l-7.3,7.3A1.222,1.222,0,0,1,487.864,447.228Z" transform="translate(-483 -437.5)" fill="#008bcf"/>
  </g>
  <g id="Group_3444" data-name="Group 3444" transform="translate(4.863 24.319)">
    <path id="Path_70" data-name="Path 70" d="M487.864,452.728a1.222,1.222,0,0,1-.861-.355l-3.648-3.648A1.218,1.218,0,1,1,485.077,447l2.787,2.789,6.435-6.437a1.218,1.218,0,1,1,1.722,1.722l-7.3,7.3A1.222,1.222,0,0,1,487.864,452.728Z" transform="translate(-483 -443)" fill="#008bcf"/>
  </g>
  <g id="Group_3445" data-name="Group 3445" transform="translate(0 4.864)">
    <path id="Path_71" data-name="Path 71" d="M504.1,483.639H482.216A1.216,1.216,0,0,1,481,482.423V436.216A1.216,1.216,0,0,1,482.216,435h34.048a1.216,1.216,0,0,1,1.216,1.216v34.048a1.225,1.225,0,0,1-.355.861l-12.16,12.16A1.224,1.224,0,0,1,504.1,483.639Zm-20.672-2.432H503.6l11.447-11.447V437.432H483.432Z" transform="translate(-481 -435)" fill="#008bcf"/>
  </g>
  <g id="Group_3446" data-name="Group 3446" transform="translate(21.888 38.912)">
    <path id="Path_72" data-name="Path 72" d="M491.216,463.592A1.216,1.216,0,0,1,490,462.376v-12.16A1.216,1.216,0,0,1,491.216,449h12.16a1.216,1.216,0,0,1,0,2.432H492.432v10.944A1.216,1.216,0,0,1,491.216,463.592Z" transform="translate(-490 -449)" fill="#008bcf"/>
  </g>
  <g id="Group_3452" data-name="Group 3452" transform="translate(41.339)">
    <g id="Group_3447" data-name="Group 3447" transform="translate(0 19.456)">
      <path id="Path_73" data-name="Path 73" d="M508.947,468.968h-9.73A1.216,1.216,0,0,1,498,467.752l0-25.536A1.216,1.216,0,0,1,499.214,441h9.73a1.216,1.216,0,0,1,1.216,1.216l0,25.536a1.212,1.212,0,0,1-1.216,1.216Zm-8.514-2.432h7.3l0-23.1h-7.3Z" transform="translate(-497.998 -441)" fill="#008bcf"/>
    </g>
    <g id="Group_3448" data-name="Group 3448" transform="translate(9.728 2.434)">
      <path id="Path_74" data-name="Path 74" d="M508.083,460.75a1.216,1.216,0,0,1-1.216-1.216l0-19.456a3.65,3.65,0,0,0-3.648-3.646,1.216,1.216,0,0,1,0-2.432,6.086,6.086,0,0,1,6.08,6.078l0,19.456A1.216,1.216,0,0,1,508.083,460.75Z" transform="translate(-501.998 -434.001)" fill="#008bcf"/>
    </g>
    <g id="Group_3449" data-name="Group 3449" transform="translate(4.869 49.855)">
      <path id="Path_75" data-name="Path 75" d="M501.216,462.012A1.216,1.216,0,0,1,500,460.8v-6.08a1.216,1.216,0,0,1,2.432,0v6.08A1.216,1.216,0,0,1,501.216,462.012Z" transform="translate(-500 -453.5)" fill="#008bcf"/>
    </g>
    <g id="Group_3450" data-name="Group 3450">
      <path id="Path_76" data-name="Path 76" d="M508.944,454.888h-9.73A1.216,1.216,0,0,1,498,453.672l0-19.456A1.216,1.216,0,0,1,499.219,433h9.725a1.217,1.217,0,0,1,1.216,1.216v19.456A1.216,1.216,0,0,1,508.944,454.888Zm-8.514-2.432h7.3V435.432h-7.294Z" transform="translate(-497.998 -433)" fill="#008bcf"/>
    </g>
    <g id="Group_3451" data-name="Group 3451" transform="translate(2.437 44.991)">
      <path id="Path_77" data-name="Path 77" d="M505.08,458.8h-4.864A1.217,1.217,0,0,1,499,457.58v-4.864a1.216,1.216,0,0,1,1.216-1.216h4.864a1.216,1.216,0,0,1,1.216,1.216v4.864A1.216,1.216,0,0,1,505.08,458.8Zm-3.648-2.432h2.432v-2.432h-2.432Z" transform="translate(-499 -451.5)" fill="#008bcf"/>
    </g>
  </g>
  <g id="Group_3453" data-name="Group 3453" transform="translate(19.456 17.024)">
    <path id="Path_78" data-name="Path 78" d="M498.728,442.432h-8.512a1.216,1.216,0,0,1,0-2.432h8.512a1.216,1.216,0,1,1,0,2.432Z" transform="translate(-489 -440)" fill="#008bcf"/>
  </g>
  <g id="Group_3454" data-name="Group 3454" transform="translate(19.456 29.184)">
    <path id="Path_79" data-name="Path 79" d="M498.728,447.432h-8.512a1.216,1.216,0,0,1,0-2.432h8.512a1.216,1.216,0,1,1,0,2.432Z" transform="translate(-489 -445)" fill="#008bcf"/>
  </g>
</svg>
