<svg id="Group_3545" data-name="Group 3545" xmlns="http://www.w3.org/2000/svg" width="45.372" height="60.496" viewBox="0 0 45.372 60.496">
  <g id="Group_3533" data-name="Group 3533" transform="translate(8.351 12.918)">
    <path id="Path_2674" data-name="Path 2674" d="M1044.826,697.66a5.521,5.521,0,0,1-5.513-5.515,1.26,1.26,0,0,1,2.521,0,2.993,2.993,0,1,0,2.992-2.992,5.514,5.514,0,1,1,5.515-5.513,1.26,1.26,0,1,1-2.521,0,2.993,2.993,0,1,0-2.994,2.992,5.514,5.514,0,1,1,0,11.028Z" transform="translate(-1039.313 -678.125)" fill="#008bcf"/>
  </g>
  <g id="Group_3534" data-name="Group 3534" transform="translate(12.603 29.933)">
    <path id="Path_2675" data-name="Path 2675" d="M1042.26,690.231a1.261,1.261,0,0,1-1.26-1.26v-2.836a1.26,1.26,0,0,1,2.521,0v2.836A1.261,1.261,0,0,1,1042.26,690.231Z" transform="translate(-1041 -684.875)" fill="#008bcf"/>
  </g>
  <g id="Group_3535" data-name="Group 3535" transform="translate(12.603 10.083)">
    <path id="Path_2676" data-name="Path 2676" d="M1042.26,682.356a1.261,1.261,0,0,1-1.26-1.26V678.26a1.26,1.26,0,1,1,2.521,0V681.1A1.261,1.261,0,0,1,1042.26,682.356Z" transform="translate(-1041 -677)" fill="#008bcf"/>
  </g>
  <g id="Group_3538" data-name="Group 3538" transform="translate(0 0)">
    <g id="Group_3536" data-name="Group 3536">
      <path id="Path_2677" data-name="Path 2677" d="M1080.112,733.5H1037.26a1.261,1.261,0,0,1-1.26-1.26V674.26a1.261,1.261,0,0,1,1.26-1.26h27.728a1.27,1.27,0,0,1,.892.368L1081,688.492a1.27,1.27,0,0,1,.368.892v42.851A1.261,1.261,0,0,1,1080.112,733.5Zm-41.591-2.521h40.331V689.906l-14.385-14.385h-25.945Z" transform="translate(-1036 -673)" fill="#008bcf"/>
    </g>
    <g id="Group_3537" data-name="Group 3537" transform="translate(27.727)">
      <path id="Path_2678" data-name="Path 2678" d="M1063.385,690.645H1048.26a1.261,1.261,0,0,1-1.26-1.26V674.26a1.26,1.26,0,0,1,2.521,0v13.864h13.864a1.26,1.26,0,0,1,0,2.521Z" transform="translate(-1047 -673)" fill="#008bcf"/>
    </g>
  </g>
  <g id="Group_3539" data-name="Group 3539" transform="translate(7.771 40.331)">
    <path id="Path_2679" data-name="Path 2679" d="M1067.862,691.521h-27.518a1.26,1.26,0,0,1,0-2.521h27.518a1.26,1.26,0,0,1,0,2.521Z" transform="translate(-1039.083 -689)" fill="#008bcf"/>
  </g>
  <g id="Group_3540" data-name="Group 3540" transform="translate(22.726 35.289)">
    <path id="Path_2680" data-name="Path 2680" d="M1058.839,689.521h-12.563a1.26,1.26,0,0,1,0-2.521h12.563a1.26,1.26,0,0,1,0,2.521Z" transform="translate(-1045.016 -687)" fill="#008bcf"/>
  </g>
  <g id="Group_3541" data-name="Group 3541" transform="translate(27.727 30.248)">
    <path id="Path_2681" data-name="Path 2681" d="M1055.823,687.521h-7.562a1.26,1.26,0,1,1,0-2.521h7.562a1.26,1.26,0,0,1,0,2.521Z" transform="translate(-1047 -685)" fill="#008bcf"/>
  </g>
  <g id="Group_3542" data-name="Group 3542" transform="translate(27.727 25.207)">
    <path id="Path_2682" data-name="Path 2682" d="M1055.823,685.521h-7.562a1.26,1.26,0,1,1,0-2.521h7.562a1.26,1.26,0,0,1,0,2.521Z" transform="translate(-1047 -683)" fill="#008bcf"/>
  </g>
  <g id="Group_3543" data-name="Group 3543" transform="translate(7.771 45.372)">
    <path id="Path_2683" data-name="Path 2683" d="M1067.862,693.521h-27.518a1.26,1.26,0,0,1,0-2.521h27.518a1.26,1.26,0,0,1,0,2.521Z" transform="translate(-1039.083 -691)" fill="#008bcf"/>
  </g>
  <g id="Group_3544" data-name="Group 3544" transform="translate(7.771 50.413)">
    <path id="Path_2684" data-name="Path 2684" d="M1067.862,695.521h-27.518a1.26,1.26,0,0,1,0-2.521h27.518a1.26,1.26,0,0,1,0,2.521Z" transform="translate(-1039.083 -693)" fill="#008bcf"/>
  </g>
</svg>
