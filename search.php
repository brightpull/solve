<?php get_header(); ?>

<div class="bg-gray-400 py-32" style="background: linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)), url('<?php echo solve_hero_img_url(); ?>'); background-position: center; background-size: cover;">

	<div class="container">

		<h1 class="text-white mb-0 md:text-6xl">Search results</h1>

	</div>

</div>

<div class="container py-20">

	<div class="grid-sidebar">

		<div class="content-area">

			<?php if ( have_posts() ) : ?>

			    <?php while ( have_posts() ) : the_post(); ?>

					<div class="border-b border-gray-400 pb-10 mb-10">

						<h1><a style="text-decoration: none;" href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h1>

						<?php the_excerpt(); ?>

					</div>

			    <?php endwhile; ?>

			<?php else : ?>

				<p>No search results. Please try again.</p>

			<?php endif; ?>

		</div>

	</div>

</div>

<?php get_footer(); ?>
