<?php echo get_template_part( 'parts/newsletter' ); ?>

<?php echo get_template_part( 'parts/sitemap' ); ?>

<?php echo get_template_part( 'parts/legal' ); ?>

<?php wp_footer(); ?>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-148611263-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-148611263-2');
</script>

</body>

</html>
