<?php get_header(); ?>

<div class="bg-gray-400 py-32" style="background: linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)), url('<?php echo get_the_post_thumbnail_url( 37 ); ?>'); background-position: center; background-size: cover;">

	<div class="container">

		<h1 class="text-white mb-0 md:text-6xl">Latest News</h1>

	</div>

</div>

<div class="container py-20">

	<div class="grid-3">

		<?php if ( have_posts() ) : ?>

		    <?php while ( have_posts() ) : the_post(); ?>

				<div>

					<a href="<?php echo get_permalink(); ?>" class="block bg-center bg-cover h-48 mb-6" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>');"></a>

					<h4 class="mb-1">
						<a href="<?php echo get_permalink(); ?>">
							<?php the_title(); ?>
						</a>
					</h4>

					<p class="uppercase text-blue font-bold text-sm"><?php the_time('F j Y'); ?></p>

					<p><?php echo get_the_excerpt(); ?> <a class="text-navy font-bold" href="<?php echo get_permalink(); ?>">Read more...</a></p>

				</div>

		    <?php endwhile; ?>

		<?php endif; ?>

	</div>

	<p class="mb-0">
		<?php previous_posts_link( 'Newer posts' ); ?>
		<?php next_posts_link( 'Older posts' ); ?>
	</p>

</div>

<?php get_footer(); ?>
