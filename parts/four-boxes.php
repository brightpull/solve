<div class="container grid-4">

	<?php if ( have_rows('box') ) : ?>

		<?php $count = 1; ?>

	    <?php while ( have_rows('box') ) : the_row(); ?>

			<div class="bg-white border-green border-b-4 text-center rounded-t-lg mb-4 pt-8 pb-4 px-6 md:mb-0">

				<div class="inline-block mb-6 flex items-center justify-center h-20">
					<?php echo get_template_part( 'svg/icon-' . $count ); ?>
				</div>

				<h4 class="text-xl"><?php the_sub_field('title'); ?></h4>

				<p class="text-sm"><?php the_sub_field('paragraph'); ?></p>

				<p>
					<a href="<?php the_sub_field('link'); ?>" class="font-bold text-xs uppercase">
						Read more
					</a>
				</p>

			</div>

			<?php $count++; ?>

	    <?php endwhile; ?>

	<?php endif; ?>
</div>
