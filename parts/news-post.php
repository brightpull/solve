<div class="border-green md:border-b-4">

	<a href="<?php echo get_permalink(); ?>" class="block bg-center bg-cover h-48 mb-6" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>');"></a>

	<h4 class="mb-1">
		<a href="<?php echo get_permalink(); ?>">
			<?php the_title(); ?>
		</a>
	</h4>

	<p class="uppercase text-blue font-bold text-sm"><?php the_time('F j Y'); ?></p>

	<p><?php echo get_the_excerpt(); ?> <a class="text-navy font-bold" href="<?php echo get_permalink(); ?>">Read more...</a></p>

</div>
