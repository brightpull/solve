<div class="bg-blue text-sm text-white py-4 hide-print">

    <div class="container flex justify-between">

        <ul class="flex items-center m-0">
            <li class="font-bold mr-4">
                <a href="tel:1300663243"><i class="mr-2 fas fa-phone opacity-50"></i>1300 663 243</a>
            </li>
            <li class="mr-2 hidden md:block">
                <a href="https://www.facebook.com/solvedisabilitysolutions" target="_blank" class="border rounded-full h-8 w-8 flex items-center justify-center hover:bg-white hover:text-blue">
                    <i class="fab fa-facebook-f fa-fw"></i>
                </a>
            </li>
            <li class="mr-2 hidden md:block">
                <a href="https://au.linkedin.com/company/solve-disability-solutions" target="_blank" class="border rounded-full h-8 w-8 flex items-center justify-center hover:bg-white hover:text-blue">
                    <i class="fab fa-linkedin-in fa-fw"></i>
                </a>
            </li>
            <li class="mr-4 hidden md:block">
                <a href="https://www.youtube.com/channel/UCTJwLU7b7B9CzOjGRN2iIYg" target="_blank" class="border rounded-full h-8 w-8 flex items-center justify-center hover:bg-white hover:text-blue">
                    <i class="fab fa-youtube fa-fw"></i>
                </a>
            </li>
            <li class="hidden md:block">
                <a href="<?php echo get_permalink( 97 ); ?>" class="button--white-b normal-case px-3 py-2 hover:bg-white hover:text-blue mb-0">
                    Easy English
                </a>
            </li>
            <li class="hidden md:block ml-2">
                <a href="https://talkify.net/text-to-speech" target="_blank" class="button--white-b normal-case px-3 py-2 hover:bg-white hover:text-blue mb-0">
                    Talkify
                </a>
            </li>
            <li class="hidden md:block ml-2">
                <a href="#" class="jq-dyslexic button--white-b normal-case px-3 py-2 hover:bg-white hover:text-blue mb-0">
                    Dyslexic Font
                </a>
            </li>
        </ul>

        <ul class="flex items-center m-0">
            <li class="mr-8 hidden md:block">
                <a href="<?php echo get_permalink( 99 ); ?>" class="button--green normal-case px-3 py-2 mb-0">
                    Request a Service
                </a>
            </li>
            <li class="font-bold mr-6 hidden md:block">
                <a href="<?php echo get_permalink( 69 ); ?>" class="hover:text-green">
                    Volunteer
                </a>
            </li>
            <li class="font-bold mr-6 hidden md:block">
                <a href="<?php echo get_permalink( 71 ); ?>" class="hover:text-green">
                    Support Us
                </a>
            </li>
            <li class="font-bold mr-6 hidden md:block">
                <a href="<?php echo get_permalink( 73 ); ?>" class="hover:text-green">
                    About Us
                </a>
            </li>
            <li class="font-bold mr-8 hidden md:block">
                <a href="<?php echo get_permalink( 75 ); ?>" class="hover:text-green">
                    Contact
                </a>
            </li>
            <li>
                <a href="#" class="jq-search bg-green text-white rounded-full h-8 w-8 flex items-center justify-center">
                    <i class="fas fa-search fa-fw"></i>
                </a>
            </li>
        </ul>

    </div>

</div>
