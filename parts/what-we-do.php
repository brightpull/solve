<div class="container grid-2 mt-20">

	<div>

		<?php if ( have_posts() ) : ?>

		    <?php while ( have_posts() ) : the_post(); ?>

		        <?php the_content(); ?>

		    <?php endwhile; ?>

		<?php endif; ?>

	</div>

	<div class="mt-10 md:mt-0">

		<div class="block bg-center bg-cover" style="background-image: url('<?php echo get_the_post_thumbnail_url($post_ID, 'full'); ?>'); height: 300px;"></div>

	</div>

</div>
