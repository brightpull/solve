<div class="c-search">

    <div class="c-search__bg hidden fixed bg-gray-900 inset-0 z-40 opacity-75"></div>

    <div class="c-search__bar absolute bg-gray-900 hidden py-8 w-full z-50 text-lg">
        <div class="container flex justify-between items-center">
            <form action="<?php echo site_url(); ?>" method="get">
                <input class="bg-gray-900 text-lg text-white c-search input focus:outline-none" type="search" name="s" placeholder="Search...">
            </form>
            <a class="jq-search text-white" href="#">
                ✕
            </a>
        </div>
    </div>

</div>
