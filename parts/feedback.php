<a href="#" class="jq-feedback c-feedback">
	Feedback
</a>

<div class="c-feedback__wrap fixed inset-0 z-40">
	<div class="c-feedback__modal">
		<div class="c-feedback__box">
			<div class="flex items-center justify-between mb-3 text-white">
				<h3 class="mb-0">We appreciate your feedback</h3>
				<a class="jq-feedback text-lg" href="#">
					✕
				</a>
			</div>
			<p class="text-white">Please fill out the form below to send us any suggestions you have.</p>
			<?php echo do_shortcode('[gravityform id=7 title=false description=false ajax=true]'); ?>
		</div>
	</div>
</div>
