<div class="text-white text-sm py-6" style="background-color: #0d1a21;">

	<div class="container text-center md:text-left md:flex justify-between">

		<p class="mb-2 md:mb-0">
			Website &copy; Solve Disability Solutions
			<a class="ml-4 mr-2" href="<?php echo get_permalink( 341 ); ?>">Legl</a>
			<a class="mr-2" href="<?php echo get_permalink( 339 ); ?>">Privacy</a>
			<a href="<?php echo get_permalink( 1203 ); ?>">Feedback and Complaints</a>
		</p>

		<p class="mb-0">Website by <a class="hover:text-green" href="https://brightagency.com.au" target="_blank">bright</a></p>

	</div>

</div>
