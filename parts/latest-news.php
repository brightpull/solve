<div class="container">

	<div class="flex justify-between mb-8">

		<h1 class="mb-0 leading-none">Latest News</h1>

		<div>
			<a href="<?php echo get_permalink( 37 ); ?>" class="button--green-b text-green mb-0">
				View All
			</a>
		</div>

	</div>

	<div class="grid-3">

		<?php query_posts( 'posts_per_page=3' ); ?>

		<?php if ( have_posts() ) : ?>

		    <?php while ( have_posts() ) : the_post(); ?>

				<div class="border-green md:border-b-4">

					<a href="<?php echo get_permalink(); ?>" class="block bg-center bg-cover h-48 mb-6" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>');"></a>

					<h4 class="mb-1">
						<a href="<?php echo get_permalink(); ?>">
							<?php the_title(); ?>
						</a>
					</h4>

					<p class="uppercase text-blue font-bold text-sm"><?php the_time('F j Y'); ?></p>

					<p><?php echo get_the_excerpt(); ?> <a class="text-navy font-bold" href="<?php echo get_permalink(); ?>">Read more...</a></p>

				</div>

		    <?php endwhile; ?>

		<?php endif; ?>

		<?php wp_reset_query(); ?>

	</div>

</div>
