<div class="bg-green text-white py-20 hide-print">

	<div class="container items-center text-center md:flex md:justify-between">

		<p class="text-2xl mb-8 md:flex md:items-center md:mb-0">
			<i class="block text-5xl fas fa-paper-plane mb-6 md:mb-0 md:mr-8"></i>
			<span>Sign up to our quarterly newsletter for the latest news from Solve.</span>
		</p>

		<div>
			<a href="#" class="button--white text-green jq-newsletter mb-0">
				Sign Up
			</a>
		</div>

	</div>

</div>
