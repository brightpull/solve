<div class="newsletter__wrap fixed inset-0 z-40">
	<div class="newsletter__modal">
		<div class="newsletter__box">
			<div class="flex items-center justify-between mb-3 text-white">
				<h3 class="mb-0">Quarterly Referrer's<br>Newsletter</h3>
				<a class="jq-newsletter text-lg" href="#">
					✕
				</a>
			</div>
			<p class="text-white">We send out a Quarterly Referrer's Newsletter with the latest news and projects from Solve. You can subscribe to the newsletter by entering your details below.</p>
			<?php echo do_shortcode('[gravityform id=1 title=false description=false ajax=true]'); ?>
		</div>
	</div>
</div>
