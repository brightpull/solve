<div class="text-center text-sm text-white py-20 hide-print" style="background-color: #162c39;">

	<div class="container grid-3">

		<div class="mb-12 md:mb-0">

			<div class="inline-block bg-center bg-cover h-24 w-24 mb-6 rounded-full" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/sitemap1.png');"></div>

			<p>NDIS Provider # 405 000 0978</p>

			<p>Find out more about Solve and the NDIS.</p>

			<p class="font-bold">You do not need to be eligible or registered with the NDIS to access Solve services.</p>

		</div>

		<div class="mb-12 md:mb-0">

			<div class="inline-block bg-center bg-cover h-24 w-24 mb-6 rounded-full" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/sitemap2.png');"></div>

			<p>If you have a hearing or speech impediment, please contact us through the National Relay Service.</p>

			<p>TTY/Voice calls <span class="font-bold">133 677</span><br>
			Speak &amp; Listen <span class="font-bold">1300 555 727</span><br>
			SMS Relay <span class="font-bold">0423 677 767</span></p>

		</div>

		<div class="mb-12 md:mb-0">

			<div class="inline-block bg-center bg-cover h-24 w-24 mb-6 rounded-full" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/sitemap3.png');"></div>

			<p>To speak to us in any language other than English please phone us via the</p>

			<p>Telephone Interpreter Service (TIS) on <span class="font-bold">13 14 50</span></p>

		</div>

	</div>

	<div class="container border-b border-white mt-10 pb-10">

		<p class="font-bold mb-0">We acknowledge and pay respects to the Elders and Traditional Owners of the land across Victoria where our services are located.</p>

	</div>

	<div class="container md:flex justify-between mt-10">

		<a href="<?php echo site_url(); ?>" class="inline-block svg-logo">
            <?php echo get_template_part( 'svg/logo-white' ); ?>
        </a>

		<div>

			<ul class="font-bold uppercase md:flex items-center">
				<li class="mr-4">
					<a href="<?php echo get_permalink( 13 ); ?>" class="hover:text-blue">
						Services
					</a>
				</li>
				<li class="mr-4">
					<a href="<?php echo get_permalink( 15 ); ?>" class="hover:text-blue">
						Assistive Technology
					</a>
				</li>
				<li class="mr-4">
					<a href="<?php echo get_permalink( 17 ); ?>" class="hover:text-blue">
						Freedom Wheels
					</a>
				</li>
				<li>
					<a href="<?php echo get_permalink( 19 ); ?>" class="hover:text-blue">
						NDIS &amp; Funding
					</a>
				</li>
			</ul>

			<ul class="m-0 p-0 flex items-center justify-center md:justify-end">
	            <li class="font-bold mr-8">
					<i class="mr-2 fas fa-phone opacity-50"></i>1300 663 243
				</li>
	            <li class="mr-2">
					<a href="https://www.facebook.com/solvedisabilitysolutions" target="_blank" class="border rounded-full h-8 w-8 flex items-center justify-center hover:bg-white hover:text-blue">
						<i class="fab fa-facebook-f fa-fw"></i>
					</a>
				</li>
	            <li class="mr-2">
					<a href="https://au.linkedin.com/company/solve-disability-solutions" target="_blank" class="border rounded-full h-8 w-8 flex items-center justify-center hover:bg-white hover:text-blue">
						<i class="fab fa-linkedin-in fa-fw"></i>
					</a>
				</li>
	            <li>
					<a href="https://www.youtube.com/channel/UCTJwLU7b7B9CzOjGRN2iIYg" target="_blank" class="border rounded-full h-8 w-8 flex items-center justify-center hover:bg-white hover:text-blue">
						<i class="fab fa-youtube fa-fw"></i>
					</a>
				</li>
	        </ul>

		</div>

	</div>

</div>
