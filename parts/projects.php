<div class="bg-blue pt-20 pb-6">

	<div class="container">

		<div class="md:flex justify-between items-center text-white">

			<div>

				<h1 class="mb-4"><?php the_field('solutions_title', 11); ?></h1>

				<p class="mb-0 text-xl"><?php the_field('solutions_paragraph', 11); ?></p>

			</div>

			<div>
				<a href="<?php echo get_permalink( 66 ); ?>" class="button--white-b mb-0 hover:text-blue">
					View All
				</a>
			</div>

		</div>

	</div>

</div>

<div class="bg-blue pb-10 projects-carousel">

	<div class="container">

		<div class="projects-carousel__wrap">

			<?php $categories = get_categories('category'); ?>

			<?php foreach ($categories as $category) : ?>

				<a href="<?php echo get_permalink( 66 ); ?>?category=<?php echo $category->term_id; ?>" class="projects-carousel__card block mb-4 md:mb-0">

					<?php $image = get_wp_term_image($category->term_id); ?>

					<div class="bg-center bg-cover h-64 rounded-t" style="background-image: url('<?php echo $image; ?>');">

					</div>

					<div class="bg-white p-6 text-center">

						<p class="font-bold text-lg mb-0"><?php echo $category->name; ?></p>

					</div>

				</a>

			<?php endforeach; ?>

		</div>

	</div>

</div>

<div class="bg-blue h-20"></div>
