<div class="hero" style="z-index: 0;">

	<?php if ( have_rows('slides') ) : ?>

		<?php while ( have_rows('slides') ) : the_row(); ?>

			<div class="py-24" style="background: linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)), url('<?php the_sub_field('background'); ?>'); background-position: center; background-size: cover;">

				<div class="container text-white hidden md:block">

					<h1 class="text-5xl md:text-7xl leading-none">
						<?php the_field('hero_title'); ?>
					</h1>

					<p class="text-xl mb-10">
						<?php the_field('hero_paragraph'); ?>
					</p>

					<p class="mb-0">
						<a href="<?php the_field('left_button_link'); ?>" class="button--blue mr-2 mb-0">
							<?php the_field('left_button_text'); ?>
						</a>
						<a href="<?php the_field('right_button_link'); ?>" class="button--green mb-0">
							<?php the_field('right_button_text'); ?>
						</a>
					</p>

					<!-- <div style="height: 40px;"></div> -->

				</div>

			</div>

		<?php endwhile; ?>

	<?php endif; ?>

</div>

<div class="py-16 bg-white md:hidden">

	<div class="container">

		<h1 class="text-5xl md:text-7xl leading-none text-blue">Live life without limits</h1>

		<p class="text-xl mb-10">We specialise in Assistive Technology built with you for you.</p>

		<p class="mb-0">
			<a href="<?php echo get_permalink( 13 ); ?>" class="button--blue mr-2 mb-0">
				What We Do
			</a>
			<a href="<?php echo get_permalink( 75 ); ?>" class="button--green mb-0">
				Get in Touch
			</a>
		</p>

		<!-- <div style="height: 40px;"></div> -->

	</div>

</div>
