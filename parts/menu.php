<div class="bg-white py-10 hide-print">

    <div class="container flex items-center justify-between">

        <a href="<?php echo site_url(); ?>" class="svg-logo">
            <?php echo get_template_part( 'svg/logo' ); ?>
        </a>

        <ul class="font-bold uppercase flex items-center m-0 p-0 links-blue hidden md:flex">
            <li class="mr-8">
                <a href="<?php echo site_url(); ?>">
                    <i class="fas fa-home"></i>
                </a>
            </li>
            <li class="mr-8">
                <a href="<?php echo get_permalink( 13 ); ?>">
                    Services
                </a>
            </li>
            <li class="mr-8">
                <a href="<?php echo get_permalink( 15 ); ?>">
                    Assistive Technology
                </a>
            </li>
            <li class="mr-8">
                <a href="<?php echo get_permalink( 17 ); ?>">
                    Freedom Wheels
                </a>
            </li>
            <li>
                <a href="<?php echo get_permalink( 19 ); ?>">
                    NDIS &amp; Funding
                </a>
            </li>
        </ul>

        <ul class="text-3xl md:hidden mb-0">
            <li>
                <a href="#" class="text-blue jq-mobile-menu">
                    <i class="fas fa-bars"></i>
                </a>
            </li>
        </ul>

    </div>

</div>

<ul class="c-mobile-menu">
    <li>
        <a href="<?php echo get_permalink( 13 ); ?>" class="uppercase">
            Services
        </a>
    </li>
    <li>
        <a href="<?php echo get_permalink( 15 ); ?>" class="uppercase">
            Assistive Technology
        </a>
    </li>
    <li>
        <a href="<?php echo get_permalink( 17 ); ?>" class="uppercase">
            Freedom Wheels
        </a>
    </li>
    <li>
        <a href="<?php echo get_permalink( 19 ); ?>" class="uppercase">
            NDIS &amp; Funding
        </a>
    </li>
    <li>
        <a href="<?php echo get_permalink( 69 ); ?>">
            Volunteer
        </a>
    </li>
    <li>
        <a href="<?php echo get_permalink( 71 ); ?>">
            Support Us
        </a>
    </li>
    <li>
        <a href="<?php echo get_permalink( 73 ); ?>">
            About Us
        </a>
    </li>
    <li>
        <a href="<?php echo get_permalink( 75 ); ?>">
            Contact
        </a>
    </li>
</ul>
