<div class="block bg-blue text-white">

	<a href="<?php echo get_permalink(); ?>" class="bg-cover bg-center block h-64 w-full" style="background-image:url('<?php echo get_the_post_thumbnail_url(); ?>')"></a>

	<div class="py-8 px-8">

		<h4 class="mb-2"><?php the_title(); ?></h4>

		<p class="text-navy font-bold text-sm uppercase"><?php the_field('category'); ?></p>

		<p class="text-sm"><?php echo get_the_excerpt(); ?></p>

		<a href="<?php echo get_permalink(); ?>" class="button--white-b hover:text-blue">
			More Info
		</a>

	</div>

</div>
