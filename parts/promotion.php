<?php if (get_field('show_promo', 11)) : ?>

<div class="container mt-10 md:mt-20">

	<div class="bg-blue text-white md:flex">

		<div class="bg-cover bg-center h-64 w-full md:h-auto md:w-1/2 p-6" style="background-image:url('<?php the_field('image', 11); ?>')">

		</div>

		<div class="px-6 py-12 md:w-1/2 md:px-10">

			<?php the_field('content', 11); ?>

			<a href="<?php the_field('button_link', 11); ?>" class="button--white-b hover:text-blue mb-0">
				<?php the_field('button_text', 11); ?>
			</a>

		</div>

	</div>

</div>

<?php endif; ?>
