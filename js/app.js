// Animate on scroll
AOS.init();

// Feedback form
jQuery('.jq-feedback').click(function(e) {
	e.preventDefault();
	jQuery('.c-feedback__wrap').fadeToggle();
});

// Project category select
jQuery('.jq-category').on('change', function () {
	var url = jQuery(this).val();
		if (url) {
			window.location = url; // redirect
		}
		return false;
});

// Newsletter signup
jQuery('.jq-newsletter').click(function(e) {
	e.preventDefault();
	jQuery('.newsletter__wrap').fadeToggle();
});

// Search bar
jQuery('.jq-search').click(function(e) {
	e.preventDefault();
	jQuery('.c-search__bar').slideToggle();
    jQuery('.c-search__bg').fadeToggle();
    jQuery('.c-search input').focus();
});

// Mobile menu
jQuery('.jq-mobile-menu').click(function(e) {
	e.preventDefault();
	jQuery('.c-mobile-menu').slideToggle();
});

// Hero on homepage
jQuery(document).ready(function(){
	jQuery('.hero').slick({
		fade: true,
		autoplay: true,
		autoplaySpeed: 3000,
		prevArrow: '<a href="#" class="slick-prev"><i class="fas fa-chevron-left"></i></a>',
		nextArrow: '<a href="#" class="slick-next"><i class="fas fa-chevron-right"></i></a>',
		responsive: [
			{
				breakpoint: 1100,
				settings: {
					arrows: false,
				}
			}
		]
	});
	jQuery('.sponsors').slick({
		infinite: true,
		slidesToShow: 6,
		slidesToScroll: 6,
		dots: true,
		responsive: [
			{
				breakpoint: 1100,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			}
		]
	});
});

// DYSLEXIC FONT
jQuery(document).ready(function() {
	if (Cookies.get('dyslexic') == 1) {
		jQuery('body').css('font-family', 'Dyslexie');
		jQuery('.jq-dyslexic').html('Standard Font');
	} else {
		jQuery('body').css('font-family', 'Museo Sans');
		jQuery('.jq-dyslexic').html('Dyslexic Font');
	}
});
jQuery('.jq-dyslexic').click(function(e) {
	e.preventDefault();
	if (Cookies.get('dyslexic') == 1) {
		Cookies.set('dyslexic', 0);
		jQuery('body').css('font-family', 'Museo Sans');
		jQuery('.container').css('max-width', '1200px');
		jQuery(this).html('Dyslexic Font');
	} else {
		Cookies.set('dyslexic', 1);
		jQuery('body').css('font-family', 'Dyslexie');
		jQuery('.container').css('max-width', '1500px');
		jQuery(this).html('Standard Font');
	}
});
