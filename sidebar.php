<div class="hide-print">

	<ul class="submenu mb-6">

		<?php

		global $post;

		$pages = get_pages('child_of=' . solve_ancestor_id());

		?>

		<?php if (count($pages) > 0) : ?>

			<li><a href="<?php echo get_permalink( solve_ancestor_id() ); ?>"><?php echo get_the_title(solve_ancestor_id()); ?></a></li>

		<?php endif; ?>

		<?php wp_list_pages('depth=1&title_li=&exclude=400,103,105,107,109,111,113,115,117,119,121&parent=' . solve_ancestor_id()); ?>

		<?php if (solve_ancestor_id() == 69) : ?>
			<?php $user = wp_get_current_user(); ?>
			<?php if ( in_array( 'volunteer', (array) $user->roles ) ) : ?>
				<?php wp_list_pages('title_li&include=105,107,109,111,113,115,117,119,121'); ?>
				<li><a href="<?php echo wp_logout_url( get_permalink(103) ); ?>">Logout</a></li>
			<?php elseif ( in_array( 'administrator', (array) $user->roles ) ) : ?>
				<?php wp_list_pages('title_li&include=105,107,109,111,113,115,117,119,121'); ?>
				<li><a href="<?php echo wp_logout_url( get_permalink(103) ); ?>">Logout</a></li>
			<?php else : ?>
				<?php wp_list_pages('title_li&include=103'); ?>
			<?php endif; ?>
		<?php endif; ?>
	</ul>

	<?php if (get_field('secondary_content')) : ?>

		<div class="bg-gray-300 mt-10 md:mt-0 px-8 py-10 content-area">

			<?php the_field('secondary_content'); ?>

		</div>

	<?php endif; ?>

</div>
